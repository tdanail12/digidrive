import React from 'react'
import { AppRegistry } from 'react-native'
import { Provider } from 'react-redux'
import App from './app/index'
import { name as appName } from './app.json'
import store from './app/config/store'
import 'react-native-gesture-handler'

const appProvider = () => {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  )
}

AppRegistry.registerComponent(appName, () => appProvider)
