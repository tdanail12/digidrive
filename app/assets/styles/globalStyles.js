const fonts = {
  $fontBold: 'Montserrat-Bold',
  $fontMedium: 'Montserrat-Medium',
  $fontRegular: 'Montserrat-Regular',
  $fontLight: 'Montserrat-Light',
}

const lightColors = {
  $colorWhite: '#ffffff',
  $colorWhiteish: '#f8f8f8',
  $colorGray: '#7e7e7e',
  $colorGrayLigth: '#efefef',

  $colorGreen: '#0bd84f',
  $colorPink: '#ed1d5d',
  $colorBlue: '#0bd8de',
  $colorBlueDark: '#1076b5',
  $colorMagenta: '#d814a9',
  $colorOrange: '#ff5800',

  $colorText: '#0d242e',

  $colorShadow: '#00347026',
  $colorBorder: '#BABABA',
  $modalTransparentColor: '#0D242Ee6',
}

export const lightStyles = {
  $theme: 'light',
  // colors
  ...fonts,
  ...lightColors,
}
