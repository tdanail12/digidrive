import EStyleSheet from 'react-native-extended-stylesheet'
import { Platform, Dimensions } from 'react-native'
const { height } = Dimensions.get('window')

export default EStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '$colorWhite',
  },
  ph: {
    paddingHorizontal: 20,
  },
  disabled: {
    opacity: 0.5,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  // TEXT
  title: {
    fontFamily: '$fontBold',
    fontSize: 22,
    lineHeight: 30,
    color: '$colorText',
  },
  subtitle: {
    fontFamily: '$fontBold',
    fontSize: 20,
    lineHeight: 28,
    color: '$colorText',
  },
  bigTextLight: {
    fontFamily: '$fontLight',
    fontSize: 18,
    lineHeight: 28,
    color: '$colorText',
  },
  bigTextBold: {
    fontFamily: '$fontBold',
    fontSize: 18,
    lineHeight: 28,
    color: '$colorText',
  },
  bigText: {
    fontFamily: '$fontRegular',
    fontSize: 18,
    lineHeight: 28,
    color: '$colorText',
  },
  bigTextMedium: {
    fontFamily: '$fontMedium',
    fontSize: 18,
    lineHeight: 28,
    color: '$colorText',
  },
  textLight: {
    fontFamily: '$fontLight',
    fontSize: 16,
    lineHeight: 22,
    color: '$colorText',
  },
  textBold: {
    fontFamily: '$fontBold',
    fontSize: 16,
    lineHeight: 22,
    color: '$colorText',
  },
  text: {
    fontFamily: '$fontRegular',
    fontSize: 16,
    lineHeight: 22,
    color: '$colorText',
  },
  textMedium: {
    fontFamily: '$fontMedium',
    fontSize: 16,
    lineHeight: 22,
    color: '$colorText',
  },
  smallTextLight: {
    fontFamily: '$fontLight',
    fontSize: 14,
    lineHeight: 20,
    color: '$colorText',
  },
  smallTextMedium: {
    fontFamily: '$fontMedium',
    fontSize: 14,
    lineHeight: 20,
    color: '$colorText',
  },
  smallText: {
    fontFamily: '$fontRegular',
    fontSize: 14,
    lineHeight: 20,
    color: '$colorText',
  },
  smallTextBold: {
    fontFamily: '$fontBold',
    fontSize: 14,
    lineHeight: 20,
    color: '$colorText',
  },
  invertArrow: {
    transform: [{ rotate: '180deg' }],
  },
  // shadow
  shadow: {
    shadowOffset: { width: 0, height: 3 },
    shadowColor: '#000000',
    shadowOpacity: 0.16,
    shadowRadius: 6,
    elevation: 2,
  },
  noShadow: {
    shadowOffset: null,
    shadowColor: null,
    shadowOpacity: null,
    shadowRadius: null,
    elevation: null,
  },
  bottomSheet: {
    wrapper: {
      backgroundColor: '$modalTransparentColor',
    },
    container: {
      backgroundColor: '$colorWhite',
      borderTopRightRadius: 15,
      borderTopLeftRadius: 15,
      justifyContent: 'center',
      alignItems: 'center',
      height: 'auto',
      paddingBottom: Platform.OS === 'ios' ? (height > 667 ? 44 : 20) : 20,
    },
    draggableIcon: {
      backgroundColor: '$colorBlue',
    },
  },
})
