import { ofType, ActionsObservable } from 'redux-observable'
import { switchMap, catchError } from 'rxjs/operators'
import { ajax } from 'rxjs/ajax'

import { sessionsTypes } from '../actions'
import { URL } from '../config/settings'
import { Headers } from '../utilities'

export const getSessions = action$ =>
  action$.pipe(
    ofType(sessionsTypes.GET_SESSIONS),
    switchMap(({ payload, onSuccess, onError }) =>
      ajax({
        url: `${URL}/sessions/schedule`,
        method: 'POST',
        headers: Headers.auth(),
        body: JSON.stringify(payload),
      }).pipe(
        switchMap(({ response }) =>
          ActionsObservable.create(obs => {
            if (onSuccess) onSuccess(response)
            obs.next({ type: sessionsTypes.GET_SESSIONS_SUCCESS, payload: response.sessions })
            obs.complete()
          }),
        ),
        catchError(err =>
          ActionsObservable.create(obs => {
            console.log(err)
            if (onError) onError(err?.response)
            obs.complete()
          }),
        ),
      ),
    ),
  )

export const getSessionsHistory = action$ =>
  action$.pipe(
    ofType(sessionsTypes.GET_SESSIONS_HISTORY),
    switchMap(({ payload, onSuccess, onError }) =>
      ajax({
        url: `${URL}/user/sessions`,
        method: 'POST',
        headers: Headers.auth(),
        body: JSON.stringify({ ...payload, limit: 10 }),
      }).pipe(
        switchMap(({ response }) =>
          ActionsObservable.create(obs => {
            if (onSuccess) onSuccess(response.sessions)
            obs.complete()
          }),
        ),
        catchError(err =>
          ActionsObservable.create(obs => {
            console.log(err)
            if (onError) onError(err?.response)
            obs.complete()
          }),
        ),
      ),
    ),
  )

export const getSession = action$ =>
  action$.pipe(
    ofType(sessionsTypes.GET_SESSION),
    switchMap(({ payload, onSuccess, onError }) =>
      ajax({
        url: `${URL}/sessions/single/${payload}`,
        method: 'GET',
        headers: Headers.auth(),
      }).pipe(
        switchMap(({ response }) =>
          ActionsObservable.create(obs => {
            if (onSuccess) onSuccess(response.sessions)
            obs.complete()
          }),
        ),
        catchError(err =>
          ActionsObservable.create(obs => {
            console.log(err)
            if (onError) onError(err?.response)
            obs.complete()
          }),
        ),
      ),
    ),
  )
