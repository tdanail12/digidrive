import { combineEpics } from 'redux-observable'
import * as auth from './auth'
import * as sessions from './sessions'

export const rootEpic = combineEpics(
  auth.requestCode,
  auth.checkCode,
  auth.logIn,
  auth.logInSuccess,
  sessions.getSessions,
  sessions.getSessionsHistory,
  sessions.getSession,
)
