import { ofType, ActionsObservable } from 'redux-observable'
import { switchMap, catchError } from 'rxjs/operators'
import { ajax } from 'rxjs/ajax'

import { authTypes } from '../actions'
import { URL } from '../config/settings'
import { Headers, Nav } from '../utilities'

export const requestCode = action$ =>
  action$.pipe(
    ofType(authTypes.REQUEST_CODE),
    switchMap(({ payload, onSuccess, onError }) =>
      ajax({
        url: `${URL}/codes`,
        method: 'POST',
        headers: Headers.get(),
        body: JSON.stringify(payload),
      }).pipe(
        switchMap(({ response }) =>
          ActionsObservable.create(obs => {
            if (onSuccess) onSuccess(response)
            obs.complete()
          }),
        ),
        catchError(err =>
          ActionsObservable.create(obs => {
            console.log(err)
            if (onError) onError(err?.response)
            obs.complete()
          }),
        ),
      ),
    ),
  )

export const checkCode = action$ =>
  action$.pipe(
    ofType(authTypes.CHECK_CODE),
    switchMap(({ payload, onSuccess, onError }) =>
      ajax({
        url: `${URL}/codes/check`,
        method: 'POST',
        headers: Headers.get(),
        body: JSON.stringify(payload),
      }).pipe(
        switchMap(({ response }) =>
          ActionsObservable.create(obs => {
            if (onSuccess) onSuccess(response)
            obs.complete()
          }),
        ),
        catchError(err =>
          ActionsObservable.create(obs => {
            console.log(err)
            if (onError) onError(err?.response)
            obs.complete()
          }),
        ),
      ),
    ),
  )

export const logIn = action$ =>
  action$.pipe(
    ofType(authTypes.LOGIN),
    switchMap(({ payload, onSuccess, onError }) =>
      ajax({
        url: `${URL}/codes/use`,
        method: 'POST',
        headers: Headers.get(),
        body: JSON.stringify(payload),
      }).pipe(
        switchMap(({ response }) =>
          ActionsObservable.create(obs => {
            obs.next({ type: authTypes.LOGIN_SUCCESS, payload: response })
            if (onSuccess) onSuccess(response)
            obs.complete()
          }),
        ),
        catchError(err =>
          ActionsObservable.create(obs => {
            console.log(err)
            if (onError) onError(err?.response)
            obs.complete()
          }),
        ),
      ),
    ),
  )

export const logInSuccess = action$ =>
  action$.pipe(
    ofType(authTypes.LOGIN_SUCCESS),
    switchMap(() =>
      ActionsObservable.create(obs => {
        Nav.resetAndNavigateTo('Tabs', {tab: 'Schedule'})
        obs.complete()
      }),
    ),
  )
