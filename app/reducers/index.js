import { combineReducers } from 'redux'
import general from './general'
import auth from './auth'
import sessions from './sessions'

export default combineReducers({
  general,
  auth,
  sessions,
})
