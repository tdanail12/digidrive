import { authTypes } from '../actions'

const initialState = {
  token: null,
  user: {},
}

const auth = (state = initialState, { type, payload }) => {
  switch (type) {
    case authTypes.LOGIN_SUCCESS:
      return { ...state, ...payload }
    case authTypes.LOGOUT:
      return initialState
    default:
      return state
  }
}

export default auth
