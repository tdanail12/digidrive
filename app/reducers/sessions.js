import { sessionsTypes } from '../actions'

const initialState = {
  sessions: [],
}

const sessions = (state = initialState, { type, payload }) => {
  switch (type) {
    case sessionsTypes.GET_SESSIONS_SUCCESS:
      return { ...state, sessions: payload }
    default:
      return state
  }
}

export default sessions
