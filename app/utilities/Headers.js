import store from '../config/store'

export class Headers {
  static get() {
    return {
      'Content-Type': 'application/json',
      language: store.getState().general.language,
    }
  }

  static auth() {
    return {
      'Content-Type': 'application/json',
      Authorization: store.getState().auth.token,
      language: store.getState().general.language,
    }
  }
}
