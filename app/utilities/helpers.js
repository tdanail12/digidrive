export const colorCodes = [
  { label: '0-30', minSpeed: 0, maxSpeed: 30, color: '#0BD84F' },
  { label: '31-60', minSpeed: 31, maxSpeed: 60, color: '#103CB5' },
  { label: '61-90', minSpeed: 61, maxSpeed: 90, color: '#FF5800' },
  { label: '91-140', minSpeed: 91, maxSpeed: 140, color: '#D80BA9' },
  { label: '>140', minSpeed: 141, maxSpeed: 500, color: '#0D242E' },
]

export const colorCodeRawRoute = rawRoute => {
  const isNewColorCode = (currentMinSpeed, currentMaxSpeed, speed) =>
    !(speed >= currentMinSpeed && speed <= currentMaxSpeed)
  const data = []
  if (rawRoute?.length) {
    let currentMinSpeed = null
    let currentMaxSpeed = null
    let prevPoint = null
    let dataIndex = 0
    rawRoute.forEach((point, index) => {
      if (index === 0) {
        const colorCode =
          colorCodes.find(item => point.speed >= item.minSpeed && point.speed <= item.maxSpeed) || colorCodes[0]
        data[dataIndex] = { ...colorCode, route: [point, point] }
        currentMinSpeed = colorCode.minSpeed
        currentMaxSpeed = colorCode.maxSpeed
      } else {
        if (isNewColorCode(currentMinSpeed, currentMaxSpeed, point.speed)) {
          dataIndex = dataIndex + 1
          const colorCode =
            colorCodes.find(item => point.speed >= item.minSpeed && point.speed <= item.maxSpeed) || colorCodes[0]
          currentMinSpeed = colorCode.minSpeed
          currentMaxSpeed = colorCode.maxSpeed
          data[dataIndex] = { ...colorCode, route: [prevPoint, point] }
        } else {
          data[dataIndex].route.push(point)
        }
      }
      prevPoint = point
    })
  }

  return data
}
