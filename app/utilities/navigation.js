import { StackActions, CommonActions, DrawerActions } from '@react-navigation/native'

let _navigator = null

export class Nav {
  static setTopLevelNavigator (navigatorRef) {
    _navigator = navigatorRef
  }

  static navigate (name, params, key) {
    _navigator.dispatch(
      CommonActions.navigate({ name, params, key })
    )
  }

  static replace (name, params) {
    _navigator.dispatch(
      StackActions.replace(name, params)
    )
  }

  static resetAndNavigateTo (name, params, key, navigator) {
    const resetParams = CommonActions.reset({
      index: 0,
      routes: [
        { name, params, key }
      ]
    })

    _navigator.dispatch(resetParams)
  }

  static goBack () {
    _navigator.dispatch(
      CommonActions.goBack()
    )
  }

  static toggleDrawer () {
    _navigator.dispatch(
      DrawerActions.toggleDrawer()
    )
  }

  static openDrawer () {
    _navigator.dispatch(
      DrawerActions.openDrawer()
    )
  }

  static closeDrawer () {
    _navigator.dispatch(
      DrawerActions.closeDrawer()
    )
  }
}

export class NavHelper {
  static getActiveRouteState (state) {
    if (
      !state.routes ||
      state.routes.length === 0 ||
      state.index >= state.routes.length
    ) {
      return state
    }

    const childActiveRoute = state.routes[state.index]
    return NavHelper.getActiveRouteState(childActiveRoute)
  }
}
