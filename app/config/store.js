import { createStore, applyMiddleware, compose } from 'redux'
// import Reactotron from './reactotron'
import { createEpicMiddleware } from 'redux-observable'
import { rootEpic } from '../epics'
import reducers from '../reducers'

const epicMiddleware = createEpicMiddleware()
let store = {}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const createStoreWithMiddleware = composeEnhancers(
  applyMiddleware(epicMiddleware),
  // Reactotron.createEnhancer()
)(createStore)

store = createStoreWithMiddleware(reducers)

epicMiddleware.run(rootEpic)

export default store
