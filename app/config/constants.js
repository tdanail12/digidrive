
export const NAV_BUTTONS = [
  {
    title: 'Кормуване',
    routeName: 'Driving',
    svgPath: 'M13.8,0C6.2,0,0,6.2,0,13.8c0,7.6,6.2,13.8,13.8,13.8c7.6,0,13.8-6.2,13.8-13.8v0C27.5,6.2,21.4,0,13.8,0zM13.8,3.4c4.4,0,8.2,2.8,9.7,6.9H4.1C5.5,6.2,9.4,3.5,13.8,3.4z M13.8,15.5c-1,0-1.7-0.8-1.7-1.7c0-1,0.8-1.7,1.7-1.7c1,0,1.7,0.8,1.7,1.7S14.7,15.5,13.8,15.5L13.8,15.5z M3.4,13.8c4.7,0,8.5,4.5,8.6,10.2C7.1,23.1,3.5,18.8,3.4,13.8z M15.5,23.9c0.1-5.6,3.9-10.2,8.6-10.2C24.1,18.8,20.5,23.1,15.5,23.9z',
    viewBox: '0 0 27.5 27.5'
  },
  {
    title: 'Профил',
    routeName: 'Profile',
    svgPath: 'M13.8,0C6.2,0,0,6.2,0,13.8c0,7.6,6.2,13.8,13.8,13.8c7.6,0,13.8-6.2,13.8-13.8C27.5,6.2,21.4,0,13.8,0L13.8,0zM13.8,4.1c2.5,0,4.6,2,4.6,4.6c0,2.5-2,4.6-4.6,4.6c-2.5,0-4.6-2-4.6-4.6C9.2,6.2,11.3,4.1,13.8,4.1L13.8,4.1z M13.8,23.9c-2.4,0-4.7-0.9-6.6-2.4c-0.4-0.4-0.7-0.9-0.7-1.5c0-2.5,2.1-4.6,4.6-4.6h0h5.3c2.5,0,4.6,2,4.6,4.6v0c0,0.6-0.2,1.1-0.7,1.5C18.5,23.1,16.2,23.9,13.8,23.9L13.8,23.9z',
    viewBox: '0 0 27.5 27.5'
  }
]

export const LOCALES = {
  monthNames: ['Януари', 'Февруари', 'Март', 'Април', 'Май', 'Юни', 'Юли', 'Август', 'Септември', 'Октомври', 'Ноември', 'Декември'],
  monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
  dayNames: ['Неделя', 'Понеделник', 'Вторник', 'Сряда', 'Четвъртък', 'Петък', 'Събота'],
  dayNamesShort: ['Нед.', 'Пон.', 'Вто.', 'Сря.', 'Чет.', 'Пет.', 'Съб.']
}

export const CALENDAR_THEME = {
  backgroundColor: '#FFFFFF', // $white
  calendarBackground: '#FFFFFF', // $white
  textSectionTitleColor: '#0BD8DE', // $activeColor
  selectedDayBackgroundColor: '#0BD8DE', // $activeColor
  selectedDayTextColor: '#FFFFFF', // $white
  todayTextColor: '#0BD84F', // $gradient[1]
  dayTextColor: '#0D242E', // $textColor
  textDisabledColor: '#BABABA', // $greyish
  dotColor: '#0D242E', // $textColor
  selectedDotColor: '#FFFFFF', // $white
  arrowColor: '#0BD8DE', // $activeColor
  monthTextColor: '#0D242E', // $textColor
  indicatorColor: '#0BD8DE', // $activeColor
  textDayFontFamily: 'Montserrat-Bold',
  textMonthFontFamily: 'Montserrat-ExtraBold',
  textDayHeaderFontFamily: 'Montserrat-ExtraBold',
  textDayFontWeight: null,
  textMonthFontWeight: null,
  textDayHeaderFontWeight: null,
  textDayFontSize: 16,
  textMonthFontSize: 27,
  textDayHeaderFontSize: 12,
  'stylesheet.day.basic': {
    base: {
      width: 40,
      height: 40,
      justifyContent: 'center',
      alignItems: 'center'
    },
    text: {
      marginTop: 2,
      fontSize: 16,
      lineHeight: 26,
      alignSelf: 'center',
      textAlign: 'center',
      fontFamily: 'Montserrat-Bold',
      fontWeight: null,
      color: '#0D242E', // $textColor
      backgroundColor: 'transparent'
    },
    selected: {
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 5,
      backgroundColor: '#0BD8DE', // $activeColor
      shadowColor: '#000000',
      shadowOffset: {
        width: 0,
        height: 3
      },
      shadowOpacity: 0.16,
      shadowRadius: 6,

      elevation: 2
    }
  }
}
