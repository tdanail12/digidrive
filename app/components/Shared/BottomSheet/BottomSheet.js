import React, { forwardRef, useRef } from 'react'
import RBSheet from 'react-native-raw-bottom-sheet'
import { Sheets } from '../../../components'
import { sharedStyles } from '../../../assets/styles'
import styles from './styles'

const BottomSheet = forwardRef(({ type, ...props }, ref) => {
  const refRBSheet = useRef(null)
  const open = () => refRBSheet.current.open()
  const close = () => refRBSheet.current.close()
  ref.current = { open, close }

  const renderContent = () => {
    switch (type) {
      case 'wrongPhoneNumber':
        return <Sheets.WrongPhoneNumber refRBSheet={{ open, close }} />
      case 'calendar':
        return <Sheets.Calendar refRBSheet={{ open, close }} {...props} />
      case 'addSession':
        return <Sheets.AddSession refRBSheet={{ open, close }} {...props} />
      default:
        return null
    }
  }

  return (
    <RBSheet
      ref={refRBSheet}
      animationType="slide"
      customStyles={sharedStyles.bottomSheet}
      closeOnDragDown={true}
      closeOnPressMask={true}>
      {renderContent()}
    </RBSheet>
  )
})

export default BottomSheet
