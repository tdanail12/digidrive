import { createIconSetFromFontello } from 'react-native-vector-icons'
import fontelloConfig from '../../../config/fontello/config.json'
const Icon = createIconSetFromFontello(fontelloConfig, fontelloConfig.name)

export default Icon
