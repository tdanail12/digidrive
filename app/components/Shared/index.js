import { default as Icon } from './Icon'
import { default as BottomSheet } from './BottomSheet'

const Shared = {
  Icon,
  BottomSheet,
}

export default Shared
