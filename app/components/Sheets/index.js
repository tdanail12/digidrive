import { default as WrongPhoneNumber } from './WrongPhoneNumber'
import { default as Calendar } from './Calendar'
import { default as AddSession } from './AddSession'

const Sheets = {
  WrongPhoneNumber,
  Calendar,
  AddSession,
}

export default Sheets
