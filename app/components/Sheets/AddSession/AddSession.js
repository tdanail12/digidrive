import React from 'react'
import { View, Text } from 'react-native'
import { Shared } from '../../../components'
import styles from './styles'

const AddSession = () => {
  return (
    <View style={[styles.container]}>
      <Text style={[styles.title]}>Моля изберете</Text>
      <View style={[styles.section, styles.sectionFirst]}>
        <View style={[styles.icon]}>
          <Shared.Icon name="driving" color="#0BD8DE" size={26} />
        </View>
        <View style={{ flex: 1 }}>
          <Text style={[styles.heading]}>Стартиране на час</Text>
          <Text style={[styles.text]}>Начало на нова сесия сега</Text>
        </View>
      </View>
      <View style={[styles.section]}>
        <View style={[styles.icon]}>
          <Shared.Icon name="add-schedule" color="#0BD8DE" size={26} />
        </View>
        <View style={{ flex: 1 }}>
          <Text style={[styles.heading]}>Добави час в график</Text>
          <Text style={[styles.text]}>Въвеждане на час за бъдещо кормуване</Text>
        </View>
      </View>
    </View>
  )
}

export default AddSession
