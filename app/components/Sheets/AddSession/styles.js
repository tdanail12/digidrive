import EStyleSheet from 'react-native-extended-stylesheet'
import { sharedStyles } from '../../../assets/styles'

export default EStyleSheet.create({
  container: {
    alignItems: 'center',
    width: '100%',
  },
  title: {
    ...sharedStyles.bigTextBold,
    textAlign: 'center',
  },
  section: {
    ...sharedStyles.row,
    paddingVertical: 20,
    width: '90%',
  },
  sectionFirst: {
    borderBottomWidth: 1,
    borderColor: '$colorBorder',
  },
  icon: {
    ...sharedStyles.shadow,
    backgroundColor: '$colorWhite',
    borderRadius: 6,
    padding: 8,
    marginRight: 15,
  },
  heading: {
    ...sharedStyles.textBold,
  },
  text: {
    ...sharedStyles.text,
  },
})
