import React, { useRef } from 'react'
import { Calendar, LocaleConfig } from 'react-native-calendars'
import { View, Text } from 'react-native'
import { LOCALES, CALENDAR_THEME } from '../../../config/constants'
import styles from './styles'

LocaleConfig.locales.bg = LOCALES
LocaleConfig.defaultLocale = 'bg'

const CalendarComp = ({ value, onChange }) => {
  const calendar = useRef(null)

  return (
    <View style={[styles.container]}>
      <View style={[styles.titleContainer]}>
        <Text style={[styles.titleText]}>Изберете дата</Text>
      </View>
      <Calendar ref={calendar} onDayPress={onChange} current={value} markedDates={{ [value]: { selected: true } }} />
    </View>
  )
}

export default CalendarComp
