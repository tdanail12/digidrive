import EStyleSheet from 'react-native-extended-stylesheet'
import { sharedStyles } from '../../../assets/styles'

export default EStyleSheet.create({
  container: {
    width: '100%',
  },
  titleContainer: {
    paddingVertical: 10,
  },
  titleText: {
    ...sharedStyles.text,
    textAlign: 'center',
  },
})
