import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import styles from './styles'

const WrongPhoneNumber = ({ refRBSheet }) => {
  return (
    <View style={[styles.container]}>
      <Text style={[styles.title]}>Нерегистриран номер</Text>
      <Text style={[styles.text]}>
        Този номер не фигурира в системата като регистриран. Може да сигнализирате екипа на DigiDrive да се свържете с
        Вашата администрация.
      </Text>
      <View style={[styles.btnSend]}>
        <Text style={[styles.btnSendText]}>Изпрати сигнал</Text>
      </View>
      <TouchableOpacity style={[styles.btnBack]} onPress={() => refRBSheet.close()}>
        <View>
          <Text style={[styles.btnBackText]}>Назад</Text>
        </View>
      </TouchableOpacity>
    </View>
  )
}

export default WrongPhoneNumber
