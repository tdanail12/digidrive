import EStyleSheet from 'react-native-extended-stylesheet'
import { sharedStyles } from '../../../assets/styles'

export default EStyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
  },
  title: {
    ...sharedStyles.bigTextBold,
    textAlign: 'center',
  },
  text: {
    ...sharedStyles.text,
    maxWidth: '90%',
    textAlign: 'center',
  },
  btnSend: {
    width: '90%',
    marginTop: 20,
    backgroundColor: '$colorBlue',
    alignItems: 'center',
    borderRadius: 6,
    paddingVertical: 5,
  },
  btnSendText: {
    ...sharedStyles.bigTextBold,
    textTransform: 'uppercase',
    color: '$colorWhite',
  },
  btnBack: {
    width: '90%',
    marginTop: 20,
    alignItems: 'center',
    borderWidth: 1,
    paddingVertical: 3,
    borderRadius: 6,
    marginBottom: 20,
  },
  btnBackText: {
    ...sharedStyles.bigTextBold,
    textTransform: 'uppercase',
  },
})
