import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { Login } from '../../../views'
import { stackOptions } from '../Main/Main'

const Stack = createStackNavigator()

const LoginNav = () => {
  return (
    <Stack.Navigator screenOptions={stackOptions}>
      <Stack.Screen name="LoginScreen" component={Login} />
    </Stack.Navigator>
  )
}

export default LoginNav
