import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { Welcome } from '../../../views'
import { stackOptions } from '../Main/Main'

const Stack = createStackNavigator()

const WelcomeNav = () => (
  <Stack.Navigator screenOptions={stackOptions}>
    <Stack.Screen name="WelcomeScreen" component={Welcome} />
  </Stack.Navigator>
)

export default WelcomeNav
