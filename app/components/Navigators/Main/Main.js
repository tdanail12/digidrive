import React from 'react'
import { Platform } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'
import { Navigators } from '../../../components'
import { History, Session } from '../../../views'

const Stack = createStackNavigator()

const Main = () => {
  return (
    <Stack.Navigator initialRouteName="Main" screenOptions={defaultOptions}>
      {/* flows */}
      <Stack.Group screenOptions={defaultOptions}>
        <Stack.Screen name="Welcome" component={Navigators.Welcome} />
        <Stack.Screen name="Login" component={Navigators.Login} />
        <Stack.Screen name="Tabs" component={Navigators.Tabs} />
      </Stack.Group>

      {/* single screens */}
      <Stack.Group screenOptions={defaultOptions}>
        <Stack.Screen name="History" component={History} />
        <Stack.Screen name="Session" component={Session} />
      </Stack.Group>

      {/* modals */}
      <Stack.Group screenOptions={modalOptions}></Stack.Group>
    </Stack.Navigator>
  )
}

const cardStyle = { flex: 1, backgroundColor: '#FFFFFF' }
export const defaultOptions = {
  headerMode: 'none',
  presentation: Platform.OS === 'ios' ? 'transparentModal' : 'card',
  cardStyle: cardStyle,
}
export const modalOptions = { headerMode: 'none', cardStyle }
export const stackOptions = { headerMode: 'none', cardStyle, presentation: 'modal' }

export default Main
