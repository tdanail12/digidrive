import React, { useState, useEffect } from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { SafeAreaView, Text, View, TouchableOpacity } from 'react-native'
import { stackOptions } from '../Main/Main'
import EStyleSheet from 'react-native-extended-stylesheet'
import { Shared } from '../../../components'
import { sharedStyles } from '../../../assets/styles'
import { Nav, NavHelper } from '../../../utilities'

import { Schedule, Profile } from '../../../views'

const Tab = createBottomTabNavigator()

const Tabs = ({ navigation }) => {
  const [activeRoute, setActiveRoute] = useState('Schedule')
  return (
    <Tab.Navigator
      screenOptions={{ ...stackOptions, headerShown: false }}
      tabBar={props => (
        <SafeAreaView
          style={[
            sharedStyles.row,
            {
              backgroundColor: EStyleSheet.value('$colorWhite'),
              justifyContent: 'space-around',
              alignItems: 'center',
              margin: 10,
            },
          ]}>
          {[
            { label: 'Кормуване', value: 'Schedule', icon: 'driving' },
            { label: 'Профил', value: 'Profile', icon: 'profile' },
          ].map(tab => {
            const isActive = activeRoute === tab.value
            return (
              <TouchableOpacity
                key={`tab-${tab.value}`}
                onPress={() => {
                  setActiveRoute(tab.value)
                  Nav.navigate(tab.value)
                }}>
                <View style={[sharedStyles.row, { opacity: isActive ? 1 : 0.5 }]}>
                  <Shared.Icon name={tab.icon} color={isActive ? '#0BD8DE' : '#BABABA'} size={30} />
                  <Text
                    style={[
                      isActive
                        ? { ...sharedStyles.textBold, textDecorationLine: 'underline', textDecorationColor: '#0BD8DE' }
                        : sharedStyles.text,
                      {
                        paddingHorizontal: 10,
                      },
                    ]}>
                    {tab.label}
                  </Text>
                </View>
              </TouchableOpacity>
            )
          })}
        </SafeAreaView>
      )}>
      <Tab.Screen name="Schedule" component={Schedule} />
      <Tab.Screen name="Profile" component={Profile} />
    </Tab.Navigator>
  )
}

export default Tabs
