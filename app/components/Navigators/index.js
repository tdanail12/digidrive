import { default as Main } from './Main'
import { default as Welcome } from './Welcome'
import { default as Tabs } from './Tabs'
import { default as Login } from './Login'

const Navigators = {
  Main,
  Welcome,
  Tabs,
  Login
}

export default Navigators
