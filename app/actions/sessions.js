export const sessionsTypes = {
  GET_SESSIONS: 'sessions/GET_SESSIONS',
  GET_SESSIONS_SUCCESS: 'sessions/GET_SESSIONS_SUCCESS',
  GET_SESSIONS_HISTORY: 'sessions/GET_SESSIONS_HISTORY',
  GET_SESSION: 'session/GET_SESSION',
}

export const getSessions = payload => ({
  type: sessionsTypes.GET_SESSIONS,
  ...payload,
})

export const getSessionsHistory = payload => ({
  type: sessionsTypes.GET_SESSIONS_HISTORY,
  ...payload,
})

export const getSession = payload => ({
  type: sessionsTypes.GET_SESSION,
  ...payload,
})
