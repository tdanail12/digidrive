import React from 'react'
import { SafeAreaView, Text, View, Image, TouchableOpacity } from 'react-native'
import { Nav } from '../../utilities'
import { sharedStyles } from '../../assets/styles'
import styles from './styles'

const Welcome = () => {
  return (
    <SafeAreaView style={[styles.container]}>
      <Image resizeMode="contain" style={[styles.logo]} source={require('../../assets/images/logo.png')} />
      <TouchableOpacity onPress={() => Nav.resetAndNavigateTo('Login')} style={[styles.button]}>
        <Text style={[styles.buttonText]}>ВХОД</Text>
      </TouchableOpacity>
      <View style={{ paddingHorizontal: '5%' }}>
        <Text style={[styles.text]}>
          Ако нямате профил, влезте на сайта <Text style={[styles.link]}>www.digidrive.bg</Text>, за да получите данни
          за достъп в приложението
        </Text>
        <Text style={[styles.text]}>
          DigiDrive предлага и{' '}
          <Text style={[styles.demo]} onPress={() => Nav.resetAndNavigateTo('Tabs')}>
            Демо Версия
          </Text>
        </Text>
      </View>
    </SafeAreaView>
  )
}
export default Welcome
