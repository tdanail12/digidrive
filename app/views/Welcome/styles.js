import EStyleSheet from 'react-native-extended-stylesheet'
import { sharedStyles } from '../../assets/styles'

export default EStyleSheet.create({
  container: {
    backgroundColor: '$colorBlueDark',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    maxWidth: '60%',
    maxHeight: '60%',
  },
  button: {
    backgroundColor: '$colorBlue',
    width: '90%',
    paddingVertical: 10,
    borderRadius: 6,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30,
  },
  buttonText: {
    ...sharedStyles.bigTextBold,
    color: '$colorWhite',
  },
  text: {
    ...sharedStyles.text,
    color: '$colorWhite',
    textAlign: 'center',
    marginVertical: 20,
  },
  link: {
    ...sharedStyles.text,
    color: '$colorWhite',
    textDecorationLine: 'underline',
  },
  demo: {
    ...sharedStyles.textBold,
    color: '$colorWhite',
    textDecorationLine: 'underline',
  },
})
