import EStyleSheet from 'react-native-extended-stylesheet'
import { sharedStyles } from '../../assets/styles'

export default EStyleSheet.create({
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  header: {
    ...sharedStyles.row,
    alignItems: 'flex-start',
    paddingHorizontal: '5%',
  },
  icon: {
    paddingVertical: 10,
    paddingRight: 10,
  },
  headerText: {
    ...sharedStyles.title,
  },
  inputContainer: {
    flex: 1,
    width: '90%',
  },
  input: {
    ...sharedStyles.textBold,
    marginTop: '30%',
    borderWidth: 1,
    borderColor: '$colorBorder',
    borderRadius: 6,
    padding: 10,
  },
  inputValid: {
    borderColor: '$colorGreen',
  },
  inputInvalid: {
    borderColor: '$colorPink',
  },
  error: {
    ...sharedStyles.smallText,
    marginTop: '5%',
    color: '$colorPink',
  },
  button: {
    ...sharedStyles.shadow,
    width: '90%',
    backgroundColor: '$colorGreen',
    marginBottom: 50,
    paddingVertical: 10,
    borderRadius: 6,
  },
  buttonDisabled: {
    opacity: 0.5,
  },
  buttonText: {
    ...sharedStyles.bigTextBold,
    color: '$colorWhite',
    textAlign: 'center',
  },
})
