import React, { useState, useEffect, useRef } from 'react'
import { SafeAreaView, View, Text, TouchableOpacity, TextInput } from 'react-native'
import { useDispatch } from 'react-redux'
import { requestCode, checkCode, logIn } from '../../actions'
import { Shared } from '../../components'
import { Nav } from '../../utilities'
import styles from './styles'

const Login = () => {
  const dispatch = useDispatch()
  const [showCode, setShowCode] = useState()
  const [phoneNumber, setPhoneNumber] = useState('+359896780237')
  const [code, setCode] = useState('111')
  const [error, setError] = useState('')
  const [isValid, setIsValid] = useState(false)
  const bottomSheet = useRef(null)

  useEffect(() => {
    if (code.length === 4) {
      dispatch(
        checkCode({
          payload: { phoneNumber, code },
          onSuccess: res => {
            setIsValid(true)
            setError('')
          },
          onError: ({ error }) => error?.code === 'ECNF001' && setError('Въведеният код е грешен'),
        }),
      )
    } else {
      setError('')
      setIsValid(false)
    }
  }, [code])

  return (
    <SafeAreaView style={[styles.container]}>
      <View style={[styles.header]}>
        <TouchableOpacity onPress={() => (showCode ? setShowCode(!showCode) : Nav.resetAndNavigateTo('Welcome'))}>
          <Shared.Icon name={'arrow-left'} style={[styles.icon]} />
        </TouchableOpacity>
        <Text style={[styles.headerText]}>
          {showCode ? 'Въведете кода, който току-що получихте' : 'Въведете Вашия мобилен Телефон'}
        </Text>
      </View>
      <View style={[styles.inputContainer]}>
        <TextInput
          style={[
            styles.input,
            (!showCode && phoneNumber) || isValid ? styles.inputValid : {},
            error ? styles.inputInvalid : {},
          ]}
          onChangeText={text => (showCode ? setCode(text) : setPhoneNumber(text))}
          defaultValue={showCode ? code : phoneNumber}
        />
        <Text style={[styles.error]}>{error}</Text>
      </View>
      <TouchableOpacity
        disabled={!phoneNumber || (showCode && !isValid)}
        style={[styles.button, !phoneNumber || (showCode && !isValid) ? styles.buttonDisabled : {}]}
        onPress={() => {
          if (isValid) dispatch(logIn({ payload: { phoneNumber, code } }))
          else if (!showCode) {
            dispatch(
              requestCode({
                payload: { phoneNumber },
                onSuccess: response => {
                  setShowCode(true)
                  setError('')
                  setIsValid(false)
                },
                onError: ({ error }) => error?.code === 'EDM001' && bottomSheet.current.open(),
              }),
            )
          }
        }}>
        <Text style={[styles.buttonText]}>Продължи</Text>
      </TouchableOpacity>
      <Shared.BottomSheet ref={bottomSheet} type="wrongPhoneNumber" />
    </SafeAreaView>
  )
}

export default Login
