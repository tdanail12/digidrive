import EStyleSheet from 'react-native-extended-stylesheet'
import { sharedStyles } from '../../assets/styles'

export default EStyleSheet.create({
  container: {
    marginHorizontal: '5%',
    width: '90%',
    flex: 1,
  },
  sectionSchoolImage: {
    height: 50,
    width: 50,
    borderRadius: 6,
  },
  sectionSchoolText: {
    marginLeft: 15,
    ...sharedStyles.subtitle,
  },
  sectionSchool: {
    ...sharedStyles.row,
    backgroundColor: '$colorWhiteish',
    height: 50,
  },
  sectionInstructor: {
    ...sharedStyles.row,
    marginVertical: 25,
  },
  sectionInstructorImage: {
    height: 100,
    width: 100,
    borderRadius: 6,
  },
  sectionInstructorText: {
    marginLeft: 15,
    ...sharedStyles.subtitle,
  },
  section: {
    ...sharedStyles.row,
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderColor: '$colorBorder',
  },
  icon: {
    ...sharedStyles.shadow,
    backgroundColor: '$colorWhite',
    borderRadius: 6,
    padding: 8,
    marginRight: 15,
  },
  heading: {
    ...sharedStyles.textBold,
  },
  text: {
    ...sharedStyles.text,
  },
})
