import React from 'react'
import { Text, SafeAreaView, View, Image, TouchableOpacity } from 'react-native'
import { useSelector } from 'react-redux'
import { Shared } from '../../components'
import { Nav } from '../../utilities'
import styles from './styles'

const Profile = () => {
  const { user } = useSelector(({ auth }) => auth)
  return (
    <SafeAreaView style={[styles.container]}>
      <View style={[styles.sectionSchool]}>
        <Image
          style={[styles.sectionSchoolImage]}
          resizeMode="cover"
          source={{ uri: user.drivingSchool.school.logo }}
        />
        <Text style={[styles.sectionSchoolText]}>{user.drivingSchool.school.name}</Text>
      </View>
      <View style={[styles.sectionInstructor]}>
        <Image style={[styles.sectionInstructorImage]} resizeMode="cover" source={{ uri: user.avatar }} />
        <Text style={[styles.sectionInstructorText]}>{user.fullName}</Text>
      </View>
      <TouchableOpacity onPress={() => Nav.navigate('History')}>
        <View style={[styles.section]}>
          <View style={[styles.icon]}>
            <Shared.Icon name="class-history" color="#0BD8DE" size={30} />
          </View>
          <View style={[{ flex: 1 }]}>
            <Text style={[styles.heading]}>История на часовете</Text>
            <Text style={[styles.text]}>Преглед на изминати кормувания</Text>
          </View>
        </View>
      </TouchableOpacity>
      <View style={[styles.section]}>
        <View style={[styles.icon]}>
          <Shared.Icon name="my-students" color="#0BD8DE" size={30} />
        </View>
        <View style={[{ flex: 1 }]}>
          <Text style={[styles.heading]}>Моите курсист</Text>
          <Text style={[styles.text]}>Преглед на всички активни курсисти</Text>
        </View>
      </View>
      <View style={[styles.section, {  alignItems: 'flex-start' }]}>
        <View style={[styles.icon]}>
          <Shared.Icon name="help" color="#0BD8DE" size={30} />
        </View>
        <View style={[{ flex: 1 }]}>
          <Text style={[styles.heading]}>Помощ</Text>
          <Text style={[styles.text]}>Връзка с екипа на DigiDrive</Text>
        </View>
      </View>
      <View style={[styles.section]}>
        <View style={[styles.icon]}>
          <Shared.Icon name="terms-conditions" color="#0BD8DE" size={30} />
        </View>
        <View style={[{ flex: 1 }]}>
          <Text style={[styles.heading]}>Условия за ползване</Text>
          <Text style={[styles.text]}>Правна информация и политики</Text>
        </View>
      </View>
      <View style={[styles.section, { borderBottomWidth: 0, marginBottom: 20 }]}>
        <View style={[styles.icon]}>
          <Shared.Icon name="log-out" color="#0D242E" size={30} />
        </View>
        <View style={[{ flex: 1 }]}>
          <Text style={[styles.heading]}>Изход</Text>
          <Text style={[styles.text]}>Излезте от Вашия профил</Text>
        </View>
      </View>
    </SafeAreaView>
  )
}

export default Profile
