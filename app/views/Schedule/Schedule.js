import React, { useState, useEffect, useRef } from 'react'
import { SafeAreaView, View, Text, ScrollView, Image, TouchableOpacity } from 'react-native'
import moment from 'moment'
import { useDispatch, useSelector } from 'react-redux'
import { times, flatten } from 'lodash'
import { getSessions } from '../../actions'
import { Shared } from '../../components'
import styles from './styles'

import 'moment/locale/bg'
moment.locale('bg')

const Schedule = () => {
  const dispatch = useDispatch()
  const bottomSheet = useRef(null)
  const scrollRef = useState(null)

  const { sessions } = useSelector(({ sessions }) => sessions)
  const [date, setDate] = useState(moment().format('YYYY-MM-DD'))
  const [sheetType, setSheetType] = useState()

  useEffect(() => {
    dispatch(
      getSessions({ payload: { start: moment(date).startOf('d').toDate(), end: moment(date).endOf('d').toDate() } }),
    )
  }, [date])

  const relevantSessions = hour => {
    const allSessions = flatten(Object.values(sessions)).filter(s => {
      const currentHour = moment(date).startOf('d').add(hour, 'h')
      return moment(s.start).isSameOrAfter(currentHour) && moment(s.start).isBefore(moment(currentHour).add(1, 'h'))
    })
    return allSessions
  }

  useEffect(() => {
    scrollRef?.current?.scrollTo({ y: moment().format('H') * 120 + moment().format('mm') * 2 - 40, animated: true })
  }, [date, sessions])

  return (
    <SafeAreaView style={[]}>
      <View style={[styles.header]}>
        <TouchableOpacity
          onPress={() => {
            setSheetType('calendar')
            bottomSheet.current.open()
          }}>
          <View style={[styles.calendar]}>
            <Shared.Icon size={26} name="calendar" color="#0BD8DE" />
            <Text style={[styles.date]}>{moment(date).format('DD MMM')}</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            setSheetType('addSession')
            bottomSheet.current.open()
          }}>
          <View style={[styles.btnAddContainer]}>
            <Shared.Icon size={21} name="plus" color="#FFFFFF" />
          </View>
        </TouchableOpacity>
      </View>
      <ScrollView style={[styles.content]} ref={scrollRef}>
        <View style={[styles.timeIndicator, { top: moment().format('H') * 120 + moment().format('mm') * 2 }]}>
          <View style={[styles.timeIndicatorInner]} />
        </View>
        {times(24)?.map(item => {
          return (
            <View key={`hour-${item}`} style={[styles.hourContainer]}>
              <View style={[styles.hourBorder]}>
                <Text style={[styles.hourText, moment().format('H') == item ? styles.hourTextCurrent : {}]}>
                  {moment().startOf('d').add(item, 'h').format('HH:00')}
                </Text>
              </View>
              {relevantSessions(item)?.map((session, n) => (
                <View
                  key={`session-${session._id}`}
                  style={[
                    styles.studentContainer,
                    {
                      top: moment(session.start).format('mm') * 2,
                      height: session.hours * 110,
                    },
                  ]}>
                  <Image style={[styles.studentImage]} source={{ uri: session.student.avatar }} />
                  <View style={[styles.studentTextContainer]}>
                    <Text style={[styles.studentName]}>{session.student.fullName}</Text>
                    <Text style={[styles.studentTimeLeft]}>Остават: 22 часа</Text>
                  </View>
                </View>
              ))}
            </View>
          )
        })}
      </ScrollView>
      <Shared.BottomSheet
        ref={bottomSheet}
        type={sheetType}
        value={date}
        onChange={({ dateString }) => setDate(dateString)}
      />
    </SafeAreaView>
  )
}

export default Schedule
