import EStyleSheet from 'react-native-extended-stylesheet'
import { sharedStyles } from '../../assets/styles'

export default EStyleSheet.create({
  container: {},
  header: {
    ...sharedStyles.row,
    marginHorizontal: '5%',
    paddingBottom: 10,
    justifyContent: 'space-between',
  },
  calendar: {
    ...sharedStyles.row,
  },
  date: {
    ...sharedStyles.title,
    color: '$colorBlue',
    marginLeft: 10,
    textTransform: 'capitalize',
  },
  btnAddContainer: {
    backgroundColor: '$colorBlue',
    overflow: 'hidden',
    borderRadius: 6,
    padding: 5,
  },
  content: {
    paddingTop: 20,
    paddingHorizontal: '5%',
  },
  hourContainer: {
    ...sharedStyles.row,
    position: 'relative',
    height: 120,
  },
  timeIndicator: {
    borderWidth: 1,
    borderColor: '$colorBlue',
    borderRadius: 50,
    height: 21,
    width: 21,
    position: 'absolute',
    top: 0,
    left: '15%',
    transform: [{ translateX: -11 }],
    zIndex: 2,

    ...sharedStyles.row,
    justifyContent: 'center',
  },
  timeIndicatorInner: {
    backgroundColor: '$colorBlue',
    overflow: 'hidden',
    borderRadius: 50,
    height: 15,
    width: 15,
    shadowColor: '$colorBlue',
    shadowOpacity: 0.5,
    shadowRadius: 10,
  },
  hourBorder: {
    height: 120,
    width: '15%',
    borderRightWidth: 1,
    borderColor: '$colorBlueDark',
  },
  hourText: {
    ...sharedStyles.textLight,
  },
  hourTextCurrent: {
    ...sharedStyles.textBold,
  },
  studentContainer: {
    position: 'absolute',
    left: '15%',
    maxWidth: '25%',
    top: 0,

    ...sharedStyles.row,
    backgroundColor: '$colorWhite',
    height: 120,
    flex: 1,
    marginLeft: 15,
    borderRadius: 6,
    shadowColor: '$colorGray',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 1,
    shadowRadius: 4,
  },
  studentImage: {
    height: '100%',
    maxWidth: '35%',
    width: 1000,
    resizeMode: 'cover',
    marginRight: 15,
    borderRadius: 6,
  },
  studentName: {
    ...sharedStyles.textBold,
    marginBottom: 15,
  },
  studentTimeLeft: {
    ...sharedStyles.text,
  },
})
