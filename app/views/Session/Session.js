import React, { useState, useEffect, useRef } from 'react'
import MapView from 'react-native-maps'
import { View, Text, TouchableOpacity } from 'react-native'
import { useDispatch } from 'react-redux'
import { getSession } from '../../actions'
import { Shared } from '../../components'
import { Nav, NavHelper, colorCodeRawRoute } from '../../utilities'
import styles from './styles'

const mapType = {
  urban: 'Градско',
  suburban: 'Извънградско',
  highway: 'Магистрала',
  night: 'Нощно',
  parking: 'Паркиране',
}
const Session = ({ navigation }) => {
  const dispatch = useDispatch()
  const { params } = NavHelper.getActiveRouteState(navigation.getState())
  const [session, setSession] = useState({})
  const mapRef = useRef(null)

  useEffect(() => {
    dispatch(getSession({ payload: params.session, onSuccess: response => setSession(response) }))
  }, [])

  const [mapReady, setMapReady] = useState(false)
  const [expanded, setExpanded] = useState(false)
  useEffect(() => {
    if (mapReady && session?.data?.length) mapRef.current.fitToCoordinates(session.data, { animated: true })
  }, [session, mapReady])

  const routeStart = session?.data?.length > 1 ? session?.data[0] : null
  const routeEnd = session?.data?.length > 1 ? session?.data[session?.data.length - 1] : null

  return (
    <View style={[styles.container]}>
      <View style={[styles.mapContainer]}>
        <MapView
          ref={mapRef}
          mapPadding={{ top: 80, bottom: 30, left: 20, right: 20 }}
          onMapReady={() => setMapReady(true)}
          style={[styles.map]}>
          {colorCodeRawRoute(session?.data)?.map((item, index) => {
            return (
              <MapView.Polyline
                key={`map-session-route-${index}-${item.label}`}
                coordinates={item.route}
                strokeWidth={4}
                strokeColor={item.color}
              />
            )
          })}
          {routeStart && (
            <MapView.Marker coordinate={routeStart}>
              <View style={[styles.routeStart]}>
                <Text style={[styles.textRoutes]}>A</Text>
              </View>
            </MapView.Marker>
          )}
          {routeEnd && (
            <MapView.Marker coordinate={routeEnd}>
              <View style={[styles.routeEnd]}>
                <Text style={[styles.textRoutes]}>B</Text>
              </View>
            </MapView.Marker>
          )}
        </MapView>
        <TouchableOpacity onPress={() => Nav.goBack()} style={{ position: 'absolute', top: 50, left: 20, zIndex: 2 }}>
          <View style={[styles.row, styles.iconContainer, styles.iconArrow]}>
            <Shared.Icon style={[styles.icon]} name="arrow-left" color="#ffffff" size={20} />
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => setExpanded(!expanded)}
          style={{ position: 'absolute', bottom: 20, right: 20, zIndex: 2 }}>
          <View style={[styles.row, styles.iconContainer, styles.iconExpand]}>
            <Shared.Icon style={[styles.icon]} name="expand" color="#ffffff" size={20} />
          </View>
        </TouchableOpacity>
      </View>
      {!expanded && (
        <View style={[styles.content]}>
          <Text style={[styles.text]}>Минати скоростни диапазони в км/ч:</Text>
          <View style={[styles.row, styles.rowSpeed]}>
            {[
              { label: '0-30', color: '#0bd84f' },
              { label: '30-60', color: '#1076b5' },
              { label: '60-90', color: '#ff5800' },
              { label: '90-140', color: '#0d242e' },
              { label: '>140', color: '#d814a9' },
            ].map(item => (
              <Text key={`speed-${item.label}`} style={[styles.speedBox, { borderColor: item?.color }]}>
                {item?.label}
              </Text>
            ))}
          </View>
          <View style={[styles.row, styles.rowDetails]}>
            <View style={[styles.grayBox]}>
              <Text style={[styles.textLight]}>Време:</Text>
              <Text style={[styles.textBold]}>{session.duration}</Text>
            </View>
            <View style={[styles.grayBox, { marginHorizontal: 10 }]}>
              <Text style={[styles.textLight]}>Скорост:</Text>
              <Text style={[styles.textBold]}>{session.avgSpeed} км/ч</Text>
            </View>
            <View style={[styles.grayBox]}>
              <Text style={[styles.textLight]}>Изминати</Text>
              <Text style={[styles.textBold]}>{session.distance} км</Text>
            </View>
          </View>

          <View style={[styles.row, styles.rowType]}>
            <View
              style={[
                styles.grayBox,
                { flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 20 },
              ]}>
              <Text style={[styles.textLight]}>Вид:</Text>
              <Text style={[styles.textBold]}>{mapType[session.type]}</Text>
            </View>
          </View>
        </View>
      )}
    </View>
  )
}

export default Session
