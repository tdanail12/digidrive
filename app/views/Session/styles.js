import EStyleSheet from 'react-native-extended-stylesheet'
import { sharedStyles } from '../../assets/styles'

export default EStyleSheet.create({
  container: {
    flex: 1,
  },
  mapContainer: {
    flex: 1,
  },
  map: {
    flex: 1,
    zIndex: 1,
  },
  routeStart: {
    width: 25,
    height: 25,
    borderRadius: 25,
    backgroundColor: '$colorBlue',
    justifyContent: 'center',
    alignItems: 'center',
  },
  routeEnd: {
    width: 25,
    height: 25,
    borderRadius: 25,
    backgroundColor: '$colorBlueDark',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textRoutes: {
    ...sharedStyles.subtitle,
    color: '$colorWhite',
    lineHeight: 24,
  },
  iconContainer: {
    backgroundColor: '$colorBlue',
    width: 35,
    height: 35,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 6,
  },
  row: { ...sharedStyles.row },
  content: {
    padding: 20,
    paddingBottom: 50,
    borderRadius: 20,
    backgroundColor: '$colorWhite',
    shadowColor: '$colorGray',
    shadowOffset: {
      width: 0,
      height: -10,
    },
    shadowOpacity: 0.3,
    shadowRadius: 5,
  },
  text: { ...sharedStyles.text },
  textLight: { ...sharedStyles.textLight },
  textBold: { ...sharedStyles.textBold },
  rowSpeed: {
    justifyContent: 'space-between',
    marginVertical: 10,
  },
  speedBox: {
    borderRadius: 6,
    borderWidth: 2,
    paddingVertical: 2,
    paddingHorizontal: 5,
    ...sharedStyles.textBold,
  },
  rowDetails: {
    justifyContent: 'space-between',
    marginVertical: 10,
  },
  grayBox: {
    backgroundColor: '$colorGrayLigth',
    paddingVertical: 5,
    borderRadius: 6,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
})
