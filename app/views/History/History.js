import React, { useState, useEffect } from 'react'
import { SafeAreaView, TouchableOpacity, View, Text, TextInput, FlatList, Image } from 'react-native'
import moment from 'moment'
import { useDispatch, useSelector } from 'react-redux'
import { getSessionsHistory } from '../../actions'
import { Shared } from '../../components'
import { Nav } from '../../utilities'
import styles from './styles'

const History = () => {
  const dispatch = useDispatch()
  const { _id: user } = useSelector(({ auth }) => auth.user)
  const [query, setQuery] = useState('')
  const [sessions, setSessions] = useState([])

  useEffect(() => {
    dispatch(getSessionsHistory({ payload: { user }, onSuccess: response => setSessions(response) }))
  }, [])

  return (
    <SafeAreaView style={[styles.container]}>
      <View style={[styles.header]}>
        <View style={[styles.headerInner]}>
          <TouchableOpacity onPress={() => Nav.goBack()}>
            <Shared.Icon name={'arrow-left'} style={[styles.icon]} />
          </TouchableOpacity>
          <Text style={[styles.headerText]}>История</Text>
        </View>
        <View style={[styles.iconFilter]}>
          <Shared.Icon name="filter" color="#ffffff" size={30} />
        </View>
      </View>
      <View style={[styles.content]}>
        <Text style={[styles.text]}>Търси по курсист</Text>
        <TextInput style={[styles.input]} onChangeText={setQuery} value={query} />
        <FlatList
          style={[styles.flatList]}
          data={sessions?.docs?.filter(({ student }) => !query || student?.fullName?.includes(query))}
          renderItem={({ item }) => (
            <View style={[styles.sessionContainer]}>
              <View style={[styles.dot]} />
              <TouchableOpacity onPress={() => Nav.navigate('Session', { session: item._id })}>
                <View style={[styles.sessionInnerContainer]}>
                  <View style={[styles.row]}>
                    <Image style={[styles.sessionImage]} source={{ uri: item.student.avatar }} />
                    <Text style={[styles.sessionName]}>{item.student.fullName}</Text>
                  </View>
                  <Text style={[styles.sessionDate]}>{moment(item.date).format('DD MMM - dddd')}</Text>
                  <View style={[styles.row, { justifyContent: 'space-between' }]}>
                    <Text style={[styles.sessionTime]}>
                      {moment(item.start).format('HH:mm')} - {moment(item.endedAt || item.end).format('HH:mm')}
                    </Text>
                    <Text style={[styles.sessionLength]}>{item.hours} Час</Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          )}
          keyExtractor={item => item._id}
        />
      </View>
    </SafeAreaView>
  )
}

export default History
