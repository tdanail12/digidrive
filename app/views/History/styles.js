import EStyleSheet from 'react-native-extended-stylesheet'
import { sharedStyles } from '../../assets/styles'

export default EStyleSheet.create({
  container: {
    width: '90%',
    marginHorizontal: '5%',
    flex: 1,
  },
  header: {
    ...sharedStyles.row,
    justifyContent: 'space-between',
  },
  headerInner: {
    ...sharedStyles.row,
  },
  headerText: {
    ...sharedStyles.title,
    marginLeft: 15,
  },
  iconFilter: {
    backgroundColor: '$colorBlue',
    padding: 5,
    borderRadius: 6,
  },
  content: {
    flex: 1,
    paddingTop: 20,
  },
  text: {
    ...sharedStyles.textLight,
  },
  input: {
    borderWidth: 1,
    borderColor: '$colorBorder',
    borderRadius: 6,
    padding: 10,
    marginVertical: 10,
  },
  flatList: {
    flex: 1,
    paddingHorizontal: 8,
  },
  sessionContainer: {
    borderLeftWidth: 1,
    borderColor: '$colorBlueDark',
    paddingLeft: 16,
    position: 'relative',
  },
  dot: {
    ...sharedStyles.shadow,
    height: 16,
    width: 16,
    backgroundColor: '$colorBlue',
    position: 'absolute',
    borderRadius: 50,
    top: '50%',
    left: -8,
    transform: [{ translateY: -5 }],
  },
  sessionInnerContainer: {
    backgroundColor: '$colorWhite',
    marginVertical: 10,
    borderRadius: 6,
    padding: 8,
    shadowColor: '$colorGray',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
  },
  row: {
    ...sharedStyles.row,
    marginVertical: 5,
  },
  sessionImage: {
    height: 50,
    width: 50,
    borderRadius: 6,
  },
  sessionName: {
    ...sharedStyles.textBold,
    marginLeft: 15,
  },
  sessionDate: {
    ...sharedStyles.textBold,
    marginVertical: 5,
    textTransform: 'capitalize',
  },
  sessionTime: { ...sharedStyles.text },
  sessionLength: { ...sharedStyles.text },
})
