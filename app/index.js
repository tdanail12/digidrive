import React, { useState, useEffect, useRef } from 'react'
import EStyleSheet from 'react-native-extended-stylesheet'
import BootSplash from 'react-native-bootsplash'
import { Animated, Dimensions, StyleSheet } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { lightStyles } from './assets/styles'
import { Navigators } from './components'
import { Nav } from './utilities'

EStyleSheet.build({ ...lightStyles })

const App = () => {
  const [bootSplashIsVisible, setBootSplashIsVisible] = useState(true)
  const [bootSplashLogoIsLoaded, setBootSplashLogoIsLoaded] = useState(false)
  const opacity = useRef(new Animated.Value(1))
  const translateY = useRef(new Animated.Value(0))

  const init = async () => {
    await new Promise(resolve => setTimeout(resolve, 1000))
    await BootSplash.hide()
    Animated.stagger(250, [
      Animated.spring(translateY.current, { useNativeDriver: true, toValue: -50 }),
      Animated.spring(translateY.current, { useNativeDriver: true, toValue: Dimensions.get('window').height }),
    ]).start()
    Animated.timing(opacity.current, { useNativeDriver: true, toValue: 0, duration: 150, delay: 350 }).start(() =>
      setBootSplashIsVisible(false),
    )
  }
  useEffect(() => {
    bootSplashLogoIsLoaded && init()
  }, [bootSplashLogoIsLoaded])

  return (
    <>
      <NavigationContainer ref={navigatorRef => Nav.setTopLevelNavigator(navigatorRef)}>
        <Navigators.Main />
      </NavigationContainer>
      {bootSplashIsVisible && (
        <Animated.View
          style={[
            StyleSheet.absoluteFill,
            {
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: EStyleSheet.value('$colorBlueDark'),
            },
            { opacity: opacity.current },
          ]}>
          <Animated.Image
            source={require('./assets/images/logo.png')}
            fadeDuration={0}
            onLoadEnd={() => setBootSplashLogoIsLoaded(true)}
            resizeMode="contain"
            style={[{ width: 200, transform: [{ translateY: translateY.current }] }]}
          />
        </Animated.View>
      )}
    </>
  )
}

export default App
